Universal Android Music Player Sample
=====================================

The goal of this sample is to show how to implement an audio media app that works
across multiple form factors and provides a consistent user experience
on Android phones, tablets, Android Auto, Android Wear, Android TV, Google Cast devices,
and with the Google Assistant.

![Screenshot showing UAMP's UI for browsing albums and songs](docs/images/custom_bottom_bar.jpg "Browse albums screenshot")
