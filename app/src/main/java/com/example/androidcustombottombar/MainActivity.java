package com.example.androidcustombottombar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.Toast;

import com.example.androidcustombottombar.adapter.MyFragmentAdapter;
import com.skydoves.androidbottombar.AndroidBottomBarView;
import com.skydoves.androidbottombar.BottomMenuItem;
import com.skydoves.androidbottombar.OnBottomMenuInitializedListener;
import com.skydoves.androidbottombar.OnMenuItemSelectedListener;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private AndroidBottomBarView androidBottomBarView;
    private ViewPager2 viewPager2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        androidBottomBarView = findViewById(R.id.bottom_bar);
        viewPager2 = findViewById(R.id.pager);
        MyFragmentAdapter adapter = new MyFragmentAdapter(this);
        viewPager2.setAdapter(adapter);
        viewPager2.setCurrentItem(1);   //why 1? because in xml we default selected 1

        setupMenu();
    }

    private void setupMenu() {
        List<BottomMenuItem> bottomMenuItemList = new ArrayList<>();
        bottomMenuItemList.add(createMenuWithBade("Home", "New", R.drawable.ic_baseline_home_24)); //badge
        bottomMenuItemList.add(createMenu("Favorites", R.drawable.ic_baseline_favorite_24));
        bottomMenuItemList.add(createMenu("Search", R.drawable.ic_baseline_search_24));
        bottomMenuItemList.add(createMenuWithBade("Friend", "99+", R.drawable.ic_baseline_person_add_24)); //badge

        //add to menu
        androidBottomBarView.addBottomMenuItems(bottomMenuItemList);
        //init menu
        androidBottomBarView.setOnBottomMenuInitializedListener(new OnBottomMenuInitializedListener() {
            @Override
            public void onInitialized() {
                androidBottomBarView.showBadge(0); //only show badge for menu have badge
                androidBottomBarView.showBadge(3);

                androidBottomBarView.bindViewPager2(viewPager2);
            }
        });

        //Event
        androidBottomBarView.setOnMenuItemSelectedListener(new OnMenuItemSelectedListener() {
            @Override
            public void onMenuItemSelected(int i, @NotNull BottomMenuItem bottomMenuItem, boolean selectFromUser) {
                if (selectFromUser) {
                    //Only fired when we selected by hand, not default select
                    androidBottomBarView.dismissBadge(i);  //dismiss badge when select
                    viewPager2.setCurrentItem(1);
                    Toast.makeText(MainActivity.this, bottomMenuItem.titleForm.getTitle(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private BottomMenuItem createMenu(String title, int resource) {
        return new BottomMenuItem(this)
                .setTitle(title)
                .setTitleColorRes(android.R.color.black)
                .setTitleActiveColorRes(android.R.color.holo_red_dark)
                .setIcon(resource)
                .setIconSize(30)
                .setIconColorRes(android.R.color.black)
                .setIconActiveColorRes(android.R.color.holo_red_dark)
                .build();
    }

    private BottomMenuItem createMenuWithBade(String title, String badge, int resource) {
        return new BottomMenuItem(this)
                .setTitle(title)
                .setTitleColorRes(android.R.color.black)
                .setTitleActiveColorRes(android.R.color.holo_red_dark)
                .setIcon(resource)
                .setIconSize(30)
                .setIconColorRes(android.R.color.black)
                .setIconActiveColorRes(android.R.color.holo_red_dark)
                .setBadgeText(badge)
                .setBadgeTextSize(9f)
                .setBadgeTextColorRes(android.R.color.white)
                .setBadgeColorRes(R.color.badge)
                .setBadgeStyle(Typeface.BOLD)
                .setBadgePadding(6)
                .setBadgeMargin(4)
                .setBadgeRadius(6)
                .build();
    }
}